package de.unirostock.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.unirostock.query.ResultSet;


public class ResultSetUtil {
	
	private static class ResultSetScoreComperator implements Comparator<ResultSet>{

		@Override
		public int compare(ResultSet rs1, ResultSet rs2) {
			if ((rs1==null) || (rs2==null)) return 0; 
			
			if (rs1.getModelScore() > rs2.getModelScore())  return 1;
			if (rs1.getModelScore() == rs2.getModelScore()) return -1;
			
			return 0;
		}
		
	}

	private static class ResultSetDatabaseIDComperator implements Comparator<ResultSet>{

		@Override
		public int compare(ResultSet rs1, ResultSet rs2) {
			if ((rs1==null) || (rs2==null)) return 0;
			
			if (rs1.getDatabaseId() > rs2.getDatabaseId())  return 1;
			if (rs1.getDatabaseId() == rs2.getDatabaseId()) return -1;
			
			return 0;
		}
		
	}
	
	public static List<ResultSet> sortByScore(List<ResultSet> rsList){

		Collections.sort(rsList, new ResultSetScoreComperator());
		return rsList;
	}
	
	public static List<ResultSet> sortByDatabaseId(List<ResultSet> rsList){

		Collections.sort(rsList, new ResultSetDatabaseIDComperator());
		return rsList;
	}
	
	public static List<ResultSet> collateByDatabaseId(List<ResultSet> rsList){
		HashMap<Long, ResultSet> toBeKept = new HashMap<Long, ResultSet>();
		for (Iterator<ResultSet> rsListIt = rsList.iterator(); rsListIt.hasNext();) {
			ResultSet resultSet = (ResultSet) rsListIt.next();
			if (toBeKept.keySet().contains(resultSet.getDatabaseId())){
				ResultSet toBeKeptResultSet = toBeKept.get(resultSet.getDatabaseId());
				if (toBeKeptResultSet.getModelScore() < resultSet.getModelScore()) toBeKept.put(resultSet.getDatabaseId(), resultSet); 
			} else {
				toBeKept.put(resultSet.getDatabaseId(), resultSet);
			}
		}
		return sortByDatabaseId(new LinkedList<ResultSet>(toBeKept.values()));
	}

	
	public static List<ResultSet> collateByModelId(List<ResultSet> rsList){
		HashMap<String, ResultSet> toBeKept = new HashMap<String, ResultSet>();
		for (Iterator<ResultSet> rsListIt = rsList.iterator(); rsListIt.hasNext();) {
			ResultSet resultSet = (ResultSet) rsListIt.next();
			if (toBeKept.keySet().contains(resultSet.getModelId())){
				ResultSet toBeKeptResultSet = toBeKept.get(resultSet.getModelId());
				if (toBeKeptResultSet.getModelScore() < resultSet.getModelScore()) toBeKept.put(resultSet.getModelId(), resultSet); 
			} else {
				toBeKept.put(resultSet.getModelId(), resultSet);
			}
		}
		return sortByScore(new LinkedList<ResultSet>(toBeKept.values()));
	}

}
