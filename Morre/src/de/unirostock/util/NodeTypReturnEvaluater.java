package de.unirostock.util;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.TraversalPosition;

import de.unirostock.configuration.Property;

public class NodeTypReturnEvaluater implements ReturnableEvaluator{
	private String nodeType;
	
	public NodeTypReturnEvaluater(String nodeType){
		this.nodeType = nodeType;
	}
	
	@Override
	public boolean isReturnableNode(TraversalPosition pos) {
		Node node = pos.currentNode();
		if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), nodeType)){	
		  return true;
		} else return false;
	}
}
