package de.unirostock.starter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.Node;

import de.unirostock.annotation.AnnotationResolverUtil;
import de.unirostock.configuration.Config;
import de.unirostock.database.Manager;
import de.unirostock.parser.SBML.SBMLExtractor;

public class MainExtractor {

	static boolean quiet = false;
	static boolean annotationOnly = false;
	static boolean noAnno = false;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String modelDir = null;
		//parse arguments
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-dbPath")) { 
				Config.dbPath = args[++i];
			}
//			if (args[i].equals("-submitter")) { 
//				Config.submitter = args[++i];
//			}
			if (args[i].equals("-directory")) { 
				modelDir = args[++i];
			}
			if (args[i].equals("-quiet")) { 
				quiet = true;
			}
			if (args[i].equals("-annoOnly")) { 
				annotationOnly = true;
			}
			if (args[i].equals("-noAnno")) { 
				noAnno = true;
			}
			
		}
		
		if (annotationOnly && noAnno) {
			System.out.print("Illegal argument combination: noAnno & annoOnly");
			return;
		}
		
		long start = System.currentTimeMillis();
		initializeDatabase();
		
		//define stdOut and a dev0
		PrintStream stdOut = new PrintStream(System.out);
		PrintStream dev0 = new PrintStream(new OutputStream() {
		    public void write(int b) {
		    }
		});
		
		if (annotationOnly) {
			System.out.println("Starting annotation index...");
			AnnotationResolverUtil.instance().fillAnnotationFullTextIndex();
			System.out.println("all done at: "+ new Date() + " needed: " + (System.currentTimeMillis()-start)+ "ms");
			return;
		}
		
		//parse and store a model
		File directory = new File(modelDir);
		int i = 0;
		for (Iterator<File> fileIt = FileUtils.iterateFiles(directory, new String[]{"xml","sbml"}, true); fileIt.hasNext();) {
			File file = (File) fileIt.next();
			if (file.isDirectory()) continue;
			
			long fileStart = System.currentTimeMillis();
			i++;
			System.out.print("Processing file# " + i +": " + file.getName() + " ...");
			if (quiet) System.setOut(dev0);
			try {
				//fake database id
				Long dID = Long.valueOf(System.nanoTime());
				Node documentNode = SBMLExtractor.extractStoreIndex(file,null,dID);
				Map<String, String> propertyMap = new HashMap<String, String>();
			    propertyMap.put("filepath", file.getPath());
			    propertyMap.put("filename", file.getName());
				SBMLExtractor.setExternalDocumentInformation(documentNode, propertyMap);
			} catch (FileNotFoundException e) {
				System.out.println("File " + file.getName() + "not found!");
				e.printStackTrace();
			} catch (XMLStreamException e) {
				System.out.println("File " + file.getName() + "caused XMLStreamException");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("File " + file.getName() + "caused IOException!");
				e.printStackTrace();
			}
			if (quiet) System.setOut(stdOut);
			System.out.println("done in " + (System.currentTimeMillis()-fileStart) + "ms");
		}
		if (!noAnno) {
			System.out.println("Starting annotation index...");
			AnnotationResolverUtil.instance().fillAnnotationFullTextIndex();
		}
		System.out.println("all done at: "+ new Date() + " needed: " + (System.currentTimeMillis()-start)+ "ms");
	}
	
	public static void initializeDatabase(){
		//create neo4j database

		System.out.println("Started at: " + new Date());
		System.out.print("Getting manager...");
		Manager.instance();
		System.out.println("done");

	}


}
