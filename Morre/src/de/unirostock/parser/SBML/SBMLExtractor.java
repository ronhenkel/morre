package de.unirostock.parser.SBML;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.CVTerm.Qualifier;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.Creator;
import org.sbml.jsbml.Event;
import org.sbml.jsbml.FunctionDefinition;
import org.sbml.jsbml.History;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ModifierSpeciesReference;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Rule;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.SpeciesReference;

import de.unirostock.configuration.Property;
import de.unirostock.configuration.Relation;
import de.unirostock.configuration.Relation.RelTypes;
import de.unirostock.data.PersonWrapper;
import de.unirostock.data.PublicationWrapper;
import de.unirostock.database.DBTraverser;
import de.unirostock.database.Manager;
import de.unirostock.query.enumerator.PersonFieldEnumerator;
import de.unirostock.query.types.PersonQuery;


public class SBMLExtractor {
	
	static SBMLReader reader = new SBMLReader();
	static GraphDatabaseService graphDB = Manager.instance().getDatabase();
	static Index<Node> nodeFullTextIndex = Manager.instance().getNodeFullTextIndex();
	static Index<Relationship> relationshipIndex = Manager.instance().getRelationshipIndex();
	static Index<Node> annotationFullTextIndex = Manager.instance().getAnnotationFullTextIndex();
	static Index<Node> publicationFullTextIndex = Manager.instance().getPublicationFullTextIndex();
	static Index<Node> personExactIndex = Manager.instance().getPersonExactIndex();

	public static Node setExternalDocumentInformation(Node documentNode, Map<String, String> propertyMap){
		if (propertyMap==null) return documentNode;
		
		for (Iterator<String> iterator = propertyMap.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty(key, propertyMap.get(key));
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			
		}
		return documentNode;
	}
	
	public static Node setExternalDocumentInformation(Node documentNode, String filepath, String filename ){
		if (StringUtils.isEmpty(filename)) filename = "";
		if (StringUtils.isEmpty(filepath)) filepath = "";
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty("filename", filename);
				documentNode.setProperty("filepath", filepath);
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			

		return documentNode;
	}
	
	public static Node extractStoreIndex(String path)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new FileInputStream(path), null, null);

	}

	public static Node extractStoreIndex(byte[] byteArray)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new ByteArrayInputStream(byteArray), null, null);
	}
	
	public static Node extractStoreIndex(File file)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new FileInputStream(file), null, null);
	}
	
	public static Node extractStoreIndex(String path, PublicationWrapper publication, Long databaseID)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new FileInputStream(path), publication, databaseID);
	}

	public static Node extractStoreIndex(byte[] byteArray, PublicationWrapper publication, Long databaseID)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new ByteArrayInputStream(byteArray), publication, databaseID);
	}
	
	public static Node extractStoreIndex(File file, PublicationWrapper publication, Long databaseID)
			throws XMLStreamException, IOException {
		return extractStoreIndex(new FileInputStream(file), publication, databaseID);
	}
	
	public static Node extractStoreIndex(InputStream stream, PublicationWrapper publication, Long databaseID) throws XMLStreamException, IOException{
		
		Node documentNode = null;
		Transaction tx = graphDB.beginTx();
		try {
			documentNode = extractFromSBML(stream, publication, databaseID);
		} catch (XMLStreamException e) {
			documentNode = null;
			//TODO Log me
			System.out.println("Error XMLStreamException while parsing model");
			System.out.println(e.getMessage());
		} finally {
			if (documentNode!=null) {
				tx.success();
			} else { 
				tx.failure();
			}
			tx.finish();
		}
		//if (!wasSuccessful) throw new XMLStreamException();
		return documentNode;
	}



	private static void processPublication(PublicationWrapper publication, Node referenceNode, Node modelNode) {
		//all Strings null -> ""
		publication.repairNullStrings();
		
		Node publicationNode = graphDB.createNode();
		publicationNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.PUBLICATION);
		publicationNode.createRelationshipTo(referenceNode, RelTypes.BELONGS_TO);
		referenceNode.createRelationshipTo(publicationNode, RelTypes.HAS_PUBLICATION);
	
		publicationNode.setProperty(Property.Publication.ABSTRACT, publication.getSynopsis());
		publicationFullTextIndex.add(publicationNode,Property.Publication.ABSTRACT, publication.getSynopsis());
		publicationNode.setProperty(Property.Publication.AFFILIATION, publication.getAffiliation());
		publicationFullTextIndex.add(publicationNode,Property.Publication.AFFILIATION, publication.getAffiliation());
		publicationNode.setProperty(Property.Publication.JOURNAL, publication.getJounral());
		publicationFullTextIndex.add(publicationNode,Property.Publication.JOURNAL, publication.getJounral());
		publicationNode.setProperty(Property.Publication.TITLE, publication.getTitle());
		publicationFullTextIndex.add(publicationNode,Property.Publication.TITLE, publication.getTitle());
		publicationNode.setProperty(Property.Publication.YEAR, publication.getYear());
		publicationFullTextIndex.add(publicationNode,Property.Publication.YEAR, publication.getYear());
		for (Iterator<PersonWrapper> iterator = publication.getAuthors().iterator(); iterator.hasNext();) {
			PersonWrapper author = (PersonWrapper) iterator.next();
			Node personNode = null;
			try {
				personNode = personExactIndex.query(Property.Person.FAMILYNAME + ":(" + author.getLastName()+ ") " 
								+Property.Person.GIVENNAME + ":(" +author.getFirstName() +")" )
								.getSingle();
				} catch (NoSuchElementException  e) {
					personNode = null;
				}					
				//create a node for each creator & link persons between models
				if (personNode==null){
					personNode= graphDB.createNode();
					personNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.PERSON);
					personNode.setProperty(Property.Person.GIVENNAME, author.getFirstName());
					personNode.setProperty(Property.Person.FAMILYNAME, author.getLastName());
					//add to person index
					personExactIndex.add(personNode, Property.Person.FAMILYNAME, author.getLastName());
					personExactIndex.add(personNode, Property.Person.GIVENNAME, author.getLastName());
					
					//add to node index
					nodeFullTextIndex.add(modelNode, Property.General.AUTHOR, author.getLastName());
					nodeFullTextIndex.add(modelNode, Property.General.AUTHOR, author.getFirstName());
				}
			
		}
		
	}

	private static void extractSBOTerm(String sboTerm, Node referenceNode){
		if (StringUtils.isEmpty(sboTerm)) return;
		
		String sboUri = "urn:miriam:biomodels.sbo:" + sboTerm;
		
		Node annotationNode = DBTraverser.fromNodeToAnnotation(referenceNode);
		
		//create if not already existing
		if (annotationNode==null) {
			annotationNode = graphDB.createNode();
			annotationNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.ANNOTATION);
			annotationNode.createRelationshipTo(referenceNode, RelTypes.BELONGS_TO);
			referenceNode.createRelationshipTo(annotationNode, RelTypes.HAS_ANNOTATION);
		}
		
		Node resource = annotationFullTextIndex.get(Property.General.URI, sboUri).getSingle();
		if (resource==null){
			resource = graphDB.createNode();
			resource.setProperty(Property.General.URI, sboUri);
			resource.setProperty(Property.General.NODE_TYPE, Property.NodeType.RESOURCE);
			annotationFullTextIndex.add(resource, Property.General.URI, sboUri);
			//old way of creating annotation index
			//annotationFullTextIndex.add(resource, Property.General.RESOURCE, AnnotationResolverUtil.getSingleURIFullText(sboUri));
		}
		//create a dynamic relationship based on the qualifier
		annotationNode.createRelationshipTo(resource, RelTypes.HAS_SBOTERM);
		resource.createRelationshipTo(annotationNode, RelTypes.BELONGS_TO);
	}

	private static void extractAnnotationNodes(Annotation annotation, Node referenceNode, Node modelNode) {
		//if no annotation is present return
		if ((annotation==null) ||  (annotation.isEmpty())) return;
	
		//create annotation node
		Node annotationNode = graphDB.createNode();
		annotationNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.ANNOTATION);
		History history = annotation.getHistory();
		
		//parse dates and creators
		if (history!=null) {
			if (history.isSetCreatedDate()) {
				annotationNode.setProperty(Property.General.CREATED, history.getCreatedDate().toString());
				nodeFullTextIndex.add(modelNode, Property.General.CREATED, history.getCreatedDate());
			}
			if (history.isSetModifiedDate()) {
				annotationNode.setProperty(Property.General.MODIFIED, history.getModifiedDate().toString());
				nodeFullTextIndex.add(modelNode, Property.General.MODIFIED, history.getModifiedDate());
			}
			if ((history.getListOfCreators() != null) && !(history.getListOfCreators().isEmpty())){
				Node personNode = null;
				for (Iterator<Creator> iterator = history.getListOfCreators().iterator(); iterator
						.hasNext();) {
					Creator creator = (Creator) iterator.next();
					try {
						PersonQuery pq = new PersonQuery();
						pq.addQueryClause(PersonFieldEnumerator.FAMILYNAME, creator.getFamilyName());
						pq.addQueryClause(PersonFieldEnumerator.GIVENNAME, creator.getGivenName());
						pq.addQueryClause(PersonFieldEnumerator.ORGANIZATION, creator.getOrganization());
						personNode = personExactIndex.query(pq.getQuery()).getSingle();
					} catch (NoSuchElementException  e) {
						personNode = null;
					}					
					//create a node for each creator & link persons between models
					if (personNode==null){
						personNode= graphDB.createNode();
						personNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.PERSON);
						personNode.setProperty(Property.Person.GIVENNAME, creator.getGivenName());
						personNode.setProperty(Property.Person.FAMILYNAME, creator.getFamilyName());
						//add to person index
						personExactIndex.add(personNode, Property.Person.FAMILYNAME, creator.getFamilyName());
						personExactIndex.add(personNode, Property.Person.GIVENNAME, creator.getGivenName());
						personExactIndex.add(personNode, Property.Person.ORGANIZATION, creator.getOrganization());
	
						//add to node index
						nodeFullTextIndex.add(modelNode, Property.General.CREATOR, creator.getGivenName());
						nodeFullTextIndex.add(modelNode, Property.General.CREATOR, creator.getFamilyName());
					}
					//set Email in case Person originates form a publication
					if (!personNode.hasProperty(Property.Person.EMAIL)){
						personNode.setProperty(Property.Person.EMAIL, creator.getEmail());
						nodeFullTextIndex.add(modelNode, Property.General.EMAIL, creator.getEmail());
						personExactIndex.add(personNode, Property.Person.EMAIL, creator.getEmail());
					}
					//set relationships
					personNode.createRelationshipTo(annotationNode, Relation.RelTypes.BELONGS_TO);
					annotationNode.createRelationshipTo(personNode, Relation.RelTypes.IS_CREATOR);
				}
				
			}	
		}
		
		//store and index non RDF 
		if (annotation.getNonRDFannotation() != null){
			annotationNode.setProperty(Property.SBML.NON_RDF, annotation.getNonRDFannotation());
			nodeFullTextIndex.add(annotationNode, Property.SBML.NON_RDF, annotation.getNonRDFannotation());
		}
		
		//get list of controlled vocabulary terms
		List<CVTerm> cvtList = annotation.getListOfCVTerms();
		for (Iterator<CVTerm> cvtIt = cvtList.iterator(); cvtIt.hasNext();) {
			CVTerm cvTerm = (CVTerm) cvtIt.next();
			Qualifier q = null;
			//identify the qualifier
			if (cvTerm.isBiologicalQualifier()){
				q = cvTerm.getBiologicalQualifierType();				
			} else if (cvTerm.isModelQualifier()){
				q = cvTerm.getModelQualifierType();
			} 
			
			//get the resources (URI) 
			List<String> resList = cvTerm.getResources();
			for (Iterator<String> resIt = resList.iterator(); resIt.hasNext();) {
				String res = (String) resIt.next();
				//test if resource already exists
				Node resource = annotationFullTextIndex.get(Property.General.URI, res).getSingle();
				if (resource==null){
					resource = graphDB.createNode();
					resource.setProperty(Property.General.URI, res);
					resource.setProperty(Property.General.NODE_TYPE, Property.NodeType.RESOURCE);
					annotationFullTextIndex.add(resource, Property.General.URI, res);
					//this is an old call method
					//annotationFullTextIndex.add(resource, Property.General.RESOURCE, AnnotationResolverUtil.getSingleURIFullText(res));
				}
				//create a dynamic relationship based on the qualifier
				annotationNode.createRelationshipTo(resource, DynamicRelationshipType.withName(q.getElementNameEquivalent()));
				resource.createRelationshipTo(annotationNode, RelTypes.BELONGS_TO);
			}
		}
	
		annotationNode.createRelationshipTo(referenceNode, RelTypes.BELONGS_TO);
		referenceNode.createRelationshipTo(annotationNode, RelTypes.HAS_ANNOTATION);
	
	}

	private static Node extractFromSBML(InputStream stream, PublicationWrapper publication, Long databaseID) throws XMLStreamException {
		
		SBMLDocument doc = null;
		Model model = null;
		doc = reader.readSBMLFromStream(stream);
		
		//create SBML document
		Node documentNode = graphDB.createNode();
		graphDB.getReferenceNode().createRelationshipTo(documentNode, RelTypes.HAS_DOCUMENT);
		//documentNode.createRelationshipTo(graphDB.getReferenceNode(), RelTypes.);
		documentNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.DOCUMENT);
		documentNode.setProperty(Property.SBML.LEVEL, doc.getLevel());
		documentNode.setProperty(Property.SBML.VERSION, doc.getVersion());
		if (databaseID!=null) documentNode.setProperty(Property.General.DATABASEID, databaseID.longValue());

		
		//create SBML model
		model = doc.getModel();
		Node modelNode = graphDB.createNode();
		documentNode.createRelationshipTo(modelNode, RelTypes.HAS_MODEL);
		modelNode.createRelationshipTo(documentNode, RelTypes.BELONGS_TO);
		modelNode.setProperty(Property.SBML.NAME, model.getName());
		modelNode.setProperty(Property.General.ID, model.getId());
		modelNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.MODEL);
		//index model name and id
		nodeFullTextIndex.add(modelNode, Property.SBML.NAME, model.getName());
		nodeFullTextIndex.add(modelNode, Property.General.ID, model.getId());

		//process annotations and link them to the model
		extractAnnotationNodes(model.getAnnotation(), modelNode, modelNode);
		//process the SBO term
		extractSBOTerm(model.getSBOTermID(), modelNode);
		//process the compartments
		Map<String, Node> compartmentNodes = extractSBMLCompartments(model.getListOfCompartments(), modelNode);
		//process the species
		Map<String, Node> speciesNodes = extractSBMLSpecies(model.getListOfSpecies(), modelNode, compartmentNodes);
		//process the reactions
		extractSBMLReactions(model.getListOfReactions(), modelNode, compartmentNodes, speciesNodes);
		//process the parameters
		extractSBMLParameters(model.getListOfParameters(), modelNode);
		//process the rules
		extractSBMLRules(model.getListOfRules(), modelNode);
		//process the events
		extractSBMLEvents(model.getListOfEvents(), modelNode);
		//process the functions
		extractSBMLFunctions(model.getListOfFunctionDefinitions(), modelNode);
		

		if (publication!=null) {
			Node annotationNode = DBTraverser.fromNodeToAnnotation(modelNode);
			if (annotationNode==null){
				annotationNode = graphDB.createNode();
				annotationNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.ANNOTATION);
				annotationNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
				modelNode.createRelationshipTo(annotationNode, RelTypes.HAS_ANNOTATION);
			} 
			processPublication(publication, annotationNode, modelNode);
				
		}
		
		return documentNode;
	}
	

	private static void extractSBMLReactions(ListOf<Reaction> listOfReaction,
			Node modelNode, Map<String, Node> compartmentList, Map<String, Node> speciesList) {

		for (Iterator<Reaction> itReac = listOfReaction.iterator(); itReac
				.hasNext();) {
			
			//iterate through reactions 
			Reaction reaction = (Reaction) itReac.next();
			Node reactionNode = graphDB.createNode();
			modelNode.createRelationshipTo(reactionNode, RelTypes.HAS_REACTION);
			reactionNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			
			reactionNode.setProperty(Property.SBML.NAME, reaction.getName());
			reactionNode.setProperty(Property.General.ID, reaction.getId());
			reactionNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.REACTION);
			
			//index mapping reaction properties to model
			nodeFullTextIndex.add(modelNode, Property.SBML.REACTION, reaction.getName());
			nodeFullTextIndex.add(modelNode, Property.SBML.REACTION, reaction.getId());
			
			//link reaction and compartment (used in SBML???)
			Node compartmentNode = compartmentList.get(reaction.getCompartment());
			if (compartmentNode!=null)
			{
				reactionNode.createRelationshipTo(compartmentNode, RelTypes.IS_LOCATED_IN);
			    compartmentNode.createRelationshipTo(reactionNode, RelTypes.CONTAINS_REACTION);
			}
			
			//link species used as modifier to reaction
			ListOf<ModifierSpeciesReference> lom = reaction.getListOfModifiers();
			for (Iterator<ModifierSpeciesReference> itLom = lom.iterator(); itLom.hasNext();) {
				ModifierSpeciesReference msr = (ModifierSpeciesReference) itLom.next();
				if ((msr.getSpecies()!=null) && speciesList.containsKey(msr.getSpecies())) {
					reactionNode.createRelationshipTo(speciesList.get(msr.getSpecies()), RelTypes.HAS_MODIFIER);
					speciesList.get(msr.getSpecies()).createRelationshipTo(reactionNode, RelTypes.IS_MODIFIER);
				}
			}
			
			//link species used as product to reaction
			ListOf<SpeciesReference> lop = reaction.getListOfProducts();
			for (Iterator<SpeciesReference> itLop = lop.iterator(); itLop.hasNext();) {
				SpeciesReference msr = (SpeciesReference) itLop.next();
				if ((msr.getSpecies()!=null) && speciesList.containsKey(msr.getSpecies())) {
					reactionNode.createRelationshipTo(speciesList.get(msr.getSpecies()), RelTypes.HAS_PRODUCT);
					speciesList.get(msr.getSpecies()).createRelationshipTo(reactionNode, RelTypes.IS_PRODUCT);
				}
			}
			
			//link species used as reactant to reaction
			ListOf<SpeciesReference> lor = reaction.getListOfReactants();
			for (Iterator<SpeciesReference> itLor = lor.iterator(); itLor.hasNext();) {
				SpeciesReference msr = (SpeciesReference) itLor.next();
				if ((msr.getSpecies()!=null) && speciesList.containsKey(msr.getSpecies())) {
					reactionNode.createRelationshipTo(speciesList.get(msr.getSpecies()), RelTypes.HAS_REACTANT);
					speciesList.get(msr.getSpecies()).createRelationshipTo(reactionNode, RelTypes.IS_REACTANT);
				}
			}
			
			//extract annotation and link to reaction node
			extractAnnotationNodes(reaction.getAnnotation(), reactionNode, modelNode);
			//process the SBO term
			extractSBOTerm(reaction.getSBOTermID(), reactionNode);
		}
	}


	private static Map<String, Node> extractSBMLSpecies(ListOf<Species> listOfSpecies,
			Node modelNode, Map<String, Node> compartmentList) {

		Map<String, Node> speciesNodes = new HashMap<String, Node>();

		for (Iterator<Species> iterator = listOfSpecies.iterator(); iterator
				.hasNext();) {
			
			//iterate through species and link back to model
			Species species = (Species) iterator.next();
			Node speciesNode = graphDB.createNode();
			modelNode.createRelationshipTo(speciesNode, RelTypes.HAS_SPECIES);
			speciesNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			
			speciesNode.setProperty(Property.SBML.NAME, species.getName());
			speciesNode.setProperty(Property.General.ID, species.getId());
			speciesNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.SPECIES);
			
			//index mapping model and species properties
			nodeFullTextIndex.add(modelNode, Property.SBML.SPECIES, species.getName());
			nodeFullTextIndex.add(modelNode, Property.SBML.SPECIES, species.getId());
			
			//create relation between compartment and species
			Node compartmentNode = compartmentList.get(species.getCompartment());
			if (compartmentNode!=null)
			{
				speciesNode.createRelationshipTo(compartmentNode, RelTypes.IS_LOCATED_IN);
			    compartmentNode.createRelationshipTo(speciesNode, RelTypes.CONTAINS_SPECIES);
			}
					
			//map species id to species for linking reactions
			speciesNodes.put(species.getId(), speciesNode);
			
			//extract annotation an link to species 
			extractAnnotationNodes(species.getAnnotation(), speciesNode, modelNode);
			//process the SBO term
			extractSBOTerm(species.getSBOTermID(), speciesNode);
		}
		return speciesNodes;
	}

	private static Map<String, Node> extractSBMLCompartments(
			ListOf<Compartment> listOfCompartments, Node modelNode) {
		Map<String, Node> compartmentNodes = new HashMap<String, Node>();
		
		for (Iterator<Compartment> iterator = listOfCompartments.iterator(); iterator
				.hasNext();) {
			
			//iterate through compartments and link back to model
			Compartment compartment = (Compartment) iterator.next();
			Node compartmentNode = graphDB.createNode();
			modelNode.createRelationshipTo(compartmentNode, RelTypes.HAS_COMPARTMENT);
			compartmentNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			
			compartmentNode.setProperty(Property.SBML.NAME, compartment.getName());
			compartmentNode.setProperty(Property.General.ID, compartment.getId());
			compartmentNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.COMPARTMENT);
			
			//index to map model to compartment properties
			nodeFullTextIndex.add(modelNode, Property.SBML.COMPARTMENT, compartment.getName());
			nodeFullTextIndex.add(modelNode, Property.SBML.COMPARTMENT, compartment.getId());
			
			//map compartment id to node for linking species
			compartmentNodes.put(compartment.getId(), compartmentNode);
			
			//extract annotation and link to compartment
			extractAnnotationNodes(compartment.getAnnotation(), compartmentNode, modelNode);
			//process the SBO term
			extractSBOTerm(compartment.getSBOTermID(), compartmentNode);
		}
		
		return compartmentNodes;
	}
	
	private static void extractSBMLParameters(
			ListOf<Parameter> listOfParameters, Node modelNode) {
		for (Iterator<Parameter> iterator = listOfParameters.iterator(); iterator
				.hasNext();) {
			Parameter parameter = (Parameter) iterator.next();
			Node parameterNode = graphDB.createNode();
			parameterNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			modelNode.createRelationshipTo(parameterNode, Relation.RelTypes.HAS_PARAMETER);
			parameterNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.PARAMETER);
			
			//extract parameter properties
			parameterNode.setProperty(Property.SBML.PARAMETER, parameter.getName());
			parameterNode.setProperty(Property.General.ID, parameter.getId());
			
			nodeFullTextIndex.add(modelNode, Property.SBML.PARAMETER, parameter.getName());
			nodeFullTextIndex.add(modelNode, Property.SBML.PARAMETER, parameter.getId());
			
			extractAnnotationNodes(parameter.getAnnotation(), parameterNode, modelNode);
			//process the SBO term
			extractSBOTerm(parameter.getSBOTermID(), parameterNode);
		}
		
	}
	

	private static void extractSBMLRules(ListOf<Rule> listOfRules,
			Node modelNode) {
		for (Iterator<Rule> iterator = listOfRules.iterator(); iterator.hasNext();) {
			Rule rule = (Rule) iterator.next();
			Node ruleNode = graphDB.createNode();
			ruleNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			modelNode.createRelationshipTo(ruleNode, RelTypes.HAS_RULE);
			ruleNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.RULE);
			
			//extract rule properties
			//TODO continue
			
			extractAnnotationNodes(rule.getAnnotation(), ruleNode, modelNode);
			//process the SBO term
			extractSBOTerm(rule.getSBOTermID(), ruleNode);
		}
		
	}
	

	private static void extractSBMLFunctions(
			ListOf<FunctionDefinition> listOfFunctionDefinitions, Node modelNode) {
		for (Iterator<FunctionDefinition> iterator = listOfFunctionDefinitions.iterator(); iterator
				.hasNext();) {
			FunctionDefinition functionDefinition = (FunctionDefinition) iterator
					.next();
			Node functionNode = graphDB.createNode();
			functionNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			modelNode.createRelationshipTo(functionNode, RelTypes.HAS_FUNCTION);
			functionNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.FUNCTION);
			//process function properties
			functionNode.setProperty(Property.SBML.FUNCTION, functionDefinition.getName());
			functionNode.setProperty(Property.General.ID, functionDefinition.getId());
			//TODO continue
			extractAnnotationNodes(functionDefinition.getAnnotation(), functionNode, modelNode);
			//process the SBO term
			extractSBOTerm(functionDefinition.getSBOTermID(), functionNode);
		}
				
	}

	private static void extractSBMLEvents(ListOf<Event> listOfEvents,
			Node modelNode) {
		for (Iterator<Event> iterator = listOfEvents.iterator(); iterator
				.hasNext();) {
			Event event = (Event) iterator.next();
			Node eventNode = graphDB.createNode();
			eventNode.createRelationshipTo(modelNode, RelTypes.BELONGS_TO);
			modelNode.createRelationshipTo(eventNode, RelTypes.HAS_EVENT);
			eventNode.setProperty(Property.General.NODE_TYPE, Property.NodeType.EVENT);
			//process event properties
			eventNode.setProperty(Property.SBML.EVENT, event.getName());
			eventNode.setProperty(Property.General.ID, event.getId());
			//TODO continue
			extractAnnotationNodes(event.getAnnotation(), eventNode, modelNode);
			//process the SBO term
			extractSBOTerm(event.getSBOTermID(), eventNode);
		}
		
		
	}
	
}
