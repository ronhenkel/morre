package de.unirostock.query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;

import de.unirostock.analyzer.AnnotationIndexAnalyzer;
import de.unirostock.analyzer.NodeFullTextIndexAnalyzer;
import de.unirostock.configuration.Property;
import de.unirostock.database.DBTraverser;
import de.unirostock.database.Manager;
import de.unirostock.query.types.InterfaceQuery;
import de.unirostock.query.types.ModelQuery;

public class QueryAdapter {

	//private static Index<Node> nodeIndex =  Manager.instance().getNodeFullTextIndex();
	//private static Index<Relationship> relationshipIndex = Manager.instance().getRelationshipIndex();
	//private static Index<Node> publicationFull =  Manager.instance().getPublicationFullTextIndex();
	//private static Index<Node> personExact =  Manager.instance().getPersonExactIndex();
	//private static Index<Node> annotationFull =  Manager.instance().getAnnotationFullTextIndex();

	public static List<ResultSet> simpleQuery(String q) throws ParseException {
		//TODO rewrite to use ModelQuery
		if (StringUtils.isEmpty(q)) return new LinkedList<ResultSet>();
		MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_31, (new ModelQuery()).getIndexedFields(), NodeFullTextIndexAnalyzer.getNodeFullTextIndexAnalyzer());
		Query query = parser.parse(q);
		Index<Node> nodeIndex= Manager.instance().getNodeFullTextIndex();
		IndexHits<Node> hits = nodeIndex.query(query);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>();

		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {		
			Node node = (Node) hitsIt.next();
			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.MODEL)){
				Node docNode = DBTraverser.fromModelToDocument(node);
				Long databaseId = null;
				if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
				ResultSet rs = new ResultSet(hits.currentScore(),(String)node.getProperty(Property.General.ID),(String)node.getProperty(Property.SBML.NAME), databaseId,null);
				result.add(rs);
			}
		}
		return result;
	}
	
	public static List<ResultSet> annotationQuery(String q, int bestN) throws ParseException {
		if (StringUtils.isEmpty(q)) return new LinkedList<ResultSet>();
		q = Property.General.RESOURCE + ":("+q+")";
		QueryParser parser = new QueryParser(Version.LUCENE_31, q, AnnotationIndexAnalyzer.getAnnotationIndexAnalyzer());
		Query query = parser.parse(q);
		Index<Node> annotationFull = Manager.instance().getAnnotationFullTextIndex();
		IndexHits<Node> hits = annotationFull.query(query);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>(); 
		int n=0;
		
		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {
			n++;
			Node annotationNode = (Node) hitsIt.next();
			result.addAll(DBTraverser.getResultSetFromNode(annotationNode, hits.currentScore()));
			if (n>=bestN) break;
		}
		return result;
	}
	
	public static List<ResultSet> executeInterfaceQuery(InterfaceQuery iq){
		return iq.getResults();
		
	}
	
	public static List<ResultSet> executeInterfaceQueries(List<InterfaceQuery> iqList){
		List<ResultSet> rs = new LinkedList<ResultSet>();
		for (Iterator<InterfaceQuery> iqIt = iqList.iterator(); iqIt.hasNext();) {
			InterfaceQuery interfaceQuery = (InterfaceQuery) iqIt.next();	
			rs.addAll(interfaceQuery.getResults());
		}
		return rs;
	}

/*
	private static Analyzer selectAnalyzerByEnum(IndexEnumerator e) {
		switch (e) {
		case MODELINDEX:
			return NodeFullTextIndexAnalyzer.getNodeFullTextIndexAnalyzer();
		case ANNOTATIONINDEX:
			return AnnotationIndexAnalyzer.getAnnotationIndexAnalyzer();
		case PUBLICATIONINDEX:
			return PublicationFullTextIndexAnalyzer.getPublicationFullTextIndexAnalyzer();
		case PERSONINDEX:
			return PersonExactIndexAnalyzer.getPersonExactIndexAnalyzer();	
		default:
			return null;
		}
	}

	private static Index<Node> selectIndexByEnum(IndexEnumerator e){
		switch (e) {
		case MODELINDEX:
			return nodeIndex;
		case ANNOTATIONINDEX:
			return annotationFull;
		case PUBLICATIONINDEX:
			return publicationFull;
		case PERSONINDEX:
			return personExact;	
		default:
			return null;
		}

	}

*/	
}
