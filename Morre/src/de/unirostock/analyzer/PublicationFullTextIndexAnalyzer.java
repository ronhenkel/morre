package de.unirostock.analyzer;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

import de.unirostock.configuration.Property;

public class PublicationFullTextIndexAnalyzer extends Analyzer{
	

	protected final static PerFieldAnalyzerWrapper publicationFullTextIndexAnalyzer =  createPublicationFullTextIndexAnalyzer();
	private final static PerFieldAnalyzerWrapper createPublicationFullTextIndexAnalyzer() {		
		Map<String, Analyzer> map = new HashMap<String, Analyzer>();
		map.put(Property.Publication.YEAR, new KeywordAnalyzer());		
		return new PerFieldAnalyzerWrapper(new StandardAnalyzer(Version.LUCENE_31), map);
	}
	


		@Override
		public TokenStream tokenStream(String fieldName, Reader reader) {
			return publicationFullTextIndexAnalyzer.tokenStream(fieldName, reader);
		}
		



	public static PerFieldAnalyzerWrapper getPublicationFullTextIndexAnalyzer() {
		return publicationFullTextIndexAnalyzer;
	}



}
